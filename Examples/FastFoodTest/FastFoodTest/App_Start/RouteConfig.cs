﻿using System.Web.Mvc;
using System.Web.Routing;

namespace FastFoodTest
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FoodMachine", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Result",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FoodMachine", action = "Result", id = UrlParameter.Optional }
            );
        }
    }
}
