﻿namespace FastFoodTest.Models
{
    public interface IButerbroadFormingableProduct : IProduct
    {
        bool IsButerbroadForming { get; set; }
    }
}
