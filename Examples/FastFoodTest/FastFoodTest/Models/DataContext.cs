﻿using System;
using System.Collections.Generic;

namespace FastFoodTest.Models
{
    public static class DataContext
    {
        public static List<IDrink> Drinks { get; set; }
        
        public static List<IProduct> DrinksAdditives { get; set; }

        public static List<IMeal> Meals { get; set; }

        public static List<IButerbroadFormingableProduct> MealsAdditives { get; set; }

        static DataContext()
        {
            RelaodData();
        }

        public static void RelaodData()
        {
            GenerateDrinksData();
            GenerateFoodData();
        }

        private static void GenerateDrinksData()
        {
            var moloko = new Product //aka clockwork orange
            {
                Guid = Guid.NewGuid(),
                Name = "Молоко",
                Coast = 10
            };

            var syrup = new Product
            {
                Guid = Guid.NewGuid(),
                Name = "Сироп",
                Coast = 5
            };

            var espresso = new Drink
            {
                Guid = Guid.NewGuid(),
                Name = "Эспрессо",
                Coast = 50,
                MilkAllowed = true
            };
            
            DrinksAdditives = new List<IProduct>
            {
                moloko,
                syrup
            };

            Drinks = new List<IDrink>()
            {
                new Drink
                {
                    Guid = Guid.NewGuid(),
                    Name = "Вода",
                    Coast = 20,
                    MilkAllowed = false
                },
                espresso,
                new Drink
                {
                    Guid = Guid.NewGuid(),
                    Name = "Латте",
                    Coast = 60,
                    MilkAllowed = true,
                    Additives = new List<IProduct>
                    {
                        espresso,
                        moloko
                    },
                },
                new Drink
                {
                    Guid = Guid.NewGuid(),
                    Name = "Капучино",
                    Coast = 70,
                    MilkAllowed = false,
                    Additives = new List<IProduct>
                    {
                        espresso,
                        moloko,
                    }
                },
                new Drink
                {
                    Guid = Guid.NewGuid(),
                    Name = "Чай черный",
                    Coast = 25,
                    MilkAllowed = true
                },
                new Drink
                {
                    Guid = Guid.NewGuid(),
                    Name = "Чай зеленый",
                    Coast = 25,
                    MilkAllowed = true
                }
            };
        }

        private static void GenerateFoodData()
        {
            var ham = new Product
            {
                Guid = Guid.NewGuid(),
                Name = "Ветчина",
                Coast = 15,
                IsButerbroadForming = true
            };

            var cheese = new Product
            {
                Guid = Guid.NewGuid(),
                Name = "Сыр",
                Coast = 10,
                IsButerbroadForming = true
            };

            var jam = new Product
            {
                Guid = Guid.NewGuid(),
                Name = "Джем",
                Coast = 7,
            };

            MealsAdditives = new List<IButerbroadFormingableProduct>
            {
                ham,
                cheese,
                jam
            };

            Meals = new List<IMeal>
            {
                new Meal
                {
                    Guid = Guid.NewGuid(),
                    Name = "Хлеб",
                    Coast = 10,
                    IsButerbroadable = true,
                },
                new Meal
                {
                    Guid = Guid.NewGuid(),
                    Name = "Булочка",
                    Coast = 15,
                    IsButerbroadable = true,
                },
                new Meal
                {
                    Guid = Guid.NewGuid(),
                    Name = "Чипсы",
                    Coast = 34,                    
                },
                new Meal
                {
                    Guid = Guid.NewGuid(),
                    Name = "Печенье",
                    Coast = 29,                    
                }
            };
        }
    }
}