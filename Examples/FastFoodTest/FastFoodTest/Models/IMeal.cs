﻿using System.Collections.Generic;

namespace FastFoodTest.Models
{
    public interface IMeal : IProduct
    { 
        bool IsButerbroadable { get; set; }
        List<IButerbroadFormingableProduct> Additives { get; set; }
    }
}
