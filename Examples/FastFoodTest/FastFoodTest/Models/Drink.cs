﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FastFoodTest.Models
{
    public class Drink : IDrink
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public int Coast { get; set; }
        public bool MilkAllowed { get; set; }

        public List<IProduct> Additives { get; set; }

        public override string ToString()
        {
            return Name + " " + GetAdditivesNames();
        }

        private string GetAdditivesNames()
        {
            var additives = this.Additives;

            if (additives == null || !additives.Any())
                return string.Empty;

            var result = additives.Aggregate("(", (x, t) => x + t + "+").TrimEnd('+') + ")";
            return result;
        }
    }
}