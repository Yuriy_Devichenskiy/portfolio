﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FastFoodTest.Models
{
    public class Meal : IMeal
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public int Coast { get; set; }
        public List<IButerbroadFormingableProduct> Additives { get; set; }
        public bool IsButerbroadable { get; set; }

        public override string ToString()
        {
            var additives = this.Additives;

            if (additives == null || !additives.Any())
                return Name;

            var result = additives.Aggregate("(", (x, t) => x + t + "+").TrimEnd('+') + ")";

            if (IsButerbroadable && additives.Any(x => x.IsButerbroadForming))
                result = "Бутерброд " + result;
            else
                result = Name + result;

            return result;
        }
    }
}