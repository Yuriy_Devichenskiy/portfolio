﻿using System.Collections.Generic;

namespace FastFoodTest.Models
{
    public interface IDrink : IProduct
    {
        bool MilkAllowed { get; set; }
        List<IProduct> Additives { get; set; }
    }
}
