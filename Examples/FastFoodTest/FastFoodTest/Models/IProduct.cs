﻿using System;

namespace FastFoodTest.Models
{
    public interface IProduct
    {
        Guid Guid { get; set; }
        string Name { get; set; }
        int Coast { get; set; }
    }
}
