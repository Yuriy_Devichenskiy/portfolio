﻿using System;

namespace FastFoodTest.Models
{
    public class Product : IButerbroadFormingableProduct
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public int Coast { get; set; }
        public bool IsButerbroadForming { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}