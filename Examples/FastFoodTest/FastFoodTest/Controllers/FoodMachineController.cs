﻿using FastFoodTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FastFoodTest.Controllers
{
    public class FoodMachineController : Controller
    {
        // GET: FoodMachine
        public ActionResult Index()
        {
            SetupViewBagData();
            //return "Hello";
            return View();
        }

        [HttpPost]
        public ActionResult Index(Guid[] selectedDrinks, Guid[] selectedDrinksAdditives, int selectedSugar, Guid[] selectedMeals, Guid[] selectedMealsAdditives, string combo)
        {
            var result = string.Empty;
            var isComboAllowed = string.IsNullOrEmpty(combo) ? false : bool.Parse(combo);
            var price = 0;

            if (selectedDrinks != null && selectedDrinks.Any())
            {
                var drink = DataContext.Drinks.Single(x => selectedDrinks.Contains(x.Guid));
                if (selectedDrinksAdditives == null) selectedDrinksAdditives = new Guid[0];
                var additives = DataContext.DrinksAdditives.Where(x => selectedDrinksAdditives.Contains(x.Guid));

                if (drink.Additives == null)
                    drink.Additives = new List<IProduct>();

                if (additives.Count() != 1)
                    isComboAllowed = false;

                foreach (var add in additives)
                    if (!drink.Additives.Contains(add))
                        drink.Additives.Add(add);

                drink.Additives.Add(new Product()
                {
                    Name = selectedSugar + "Сахар",
                    Coast = 3 * selectedSugar,
                });

                result += "Напиток: " + drink;
                price = drink.Coast + additives.Sum(x => x.Coast) + selectedSugar * 3;
            }
            else
                isComboAllowed = false;

            if (selectedMeals != null && selectedMeals.Any())
            {
                var meal = DataContext.Meals.Single(x => selectedMeals.Contains(x.Guid));
                if (selectedMealsAdditives == null) selectedMealsAdditives = new Guid[0];
                var mealsAdditives = DataContext.MealsAdditives.Where(x => selectedMealsAdditives.Contains(x.Guid));

                if (meal.Additives == null)
                    meal.Additives = new List<IButerbroadFormingableProduct>();

                if (mealsAdditives.Count() != 1)
                    isComboAllowed = false;

                foreach (var add in mealsAdditives)
                    if (!meal.Additives.Contains(add))
                        meal.Additives.Add(add);
                if (!string.IsNullOrEmpty(result))
                    result += "\n";

                result += "Еда: " + meal;
                price += meal.Coast + mealsAdditives.Sum(x => x.Coast);
            }
            else
                isComboAllowed = false;

            if (isComboAllowed)
            {
                result += " (Комбо) ";
                price = 90;                    
            }

            if (!string.IsNullOrEmpty(result))
                ViewBag.Message = result + " " + price + "руб.";
            else
                ViewBag.Message = "Ничего не выбрано!";

            DataContext.RelaodData();

            return View("Result");
        }

        public void SetupViewBagData()
        {
            ViewBag.Drinks = DataContext.Drinks;
            ViewBag.DrinksAdditives = DataContext.DrinksAdditives;
            ViewBag.Meals = DataContext.Meals;
            ViewBag.MealsAdditives = DataContext.MealsAdditives;
        }
    }
}