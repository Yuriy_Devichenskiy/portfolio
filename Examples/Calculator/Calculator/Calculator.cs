﻿using System.Text.RegularExpressions;

namespace Calculator
{
    internal class Calculator
    {
        private static readonly char[] MulDivChars = { '*', '/' };

        public string Calcuclate(string input)
        {
            input = input.Replace(" ", string.Empty);
            input = input.Replace(".", ",");
            
            while (true)
            {
                var regEx = new Regex(@"\(([^\(\)]+)\)");
                if (!regEx.IsMatch(input))
                {
                    TryOpertations(ref input);
                    return input;
                }

                var match = regEx.Match(input);

                var matchGrStr = match.Groups[1].ToString();
                
                TryOpertations(ref matchGrStr);

                input = input.Replace(match.Groups[0].ToString(), matchGrStr);
            }
        }

        protected void TryOpertations(ref string input)
        {
            while (TryMultOrDiv(ref input)) { }
            while (TryAddOrRem(ref input)) { }
        }

        private bool TryMultOrDiv(ref string input)
        {
            if (input.IndexOfAny(MulDivChars) == -1) return false;

            var regEx = new Regex(@"(\-*[\d,]+)([\*\/]{1})(\-*[\d,]+)");

            var match = regEx.Match(input);

            if (match.Groups.Count == 4)
            {
                var leftOper = double.Parse(match.Groups[1].ToString());
                var rightOper = double.Parse(match.Groups[3].ToString());

                if (match.Groups[2].ToString()[0] == '*')
                    leftOper *= rightOper;
                else if (match.Groups[2].ToString()[0] == '/')
                    leftOper /= rightOper;

                input = input.Replace(match.Groups[0].ToString(), leftOper.ToString());
            }

            return true;
        }

        private bool TryAddOrRem(ref string input)
        {
            var regEx = new Regex(@"^(\-*[\d,]+)$");

            if (regEx.IsMatch(input)) return false;

            var reg = new Regex(@"(\-*[\d,]+)([\+\-]{1})(\-*[\d,]+)");
            var match = reg.Match(input);

            if (match.Groups.Count > 2)
            {
                var leftOper = double.Parse(match.Groups[1].ToString());
                var rightOper = double.Parse(match.Groups[3].ToString());

                if (match.Groups[2].ToString()[0] == '+')
                    leftOper += rightOper;
                else if (match.Groups[2].ToString()[0] == '-')
                    leftOper -= rightOper;
                input = input.Replace(match.Groups[0].ToString(), leftOper.ToString());
            }
            return true;
        }
    }
}
