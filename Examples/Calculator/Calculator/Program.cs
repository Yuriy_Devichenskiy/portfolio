﻿using System;
using System.Text.RegularExpressions;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var calc = new Calculator();
            while (true)
            {
                Console.WriteLine("Введите выражение:");

                var input = Console.ReadLine();
                if (Regex.IsMatch(input, @"[А-ЯЁ]") || Regex.IsMatch(input, @"[а-яё]") ||
                    Regex.IsMatch(input, @"[A-Z]") || Regex.IsMatch(input, @"[a-z]"))
                {
                    Console.WriteLine("Вырежение не должно содержать букв!");
                    continue;
                }

                Console.WriteLine("Ответ: {0}.", calc.Calcuclate(input));
            }
        }
    }
}
