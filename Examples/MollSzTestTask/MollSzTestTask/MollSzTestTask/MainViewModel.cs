﻿using System.Collections.ObjectModel;
using System.Linq;

namespace MollSzTestTask
{
    public class MainViewModel
    {
        public ObservableCollection<CustomTabItem> CustomTabs { get; set; }

        public MainViewModel()
        {
            CustomTabs = new ObservableCollection<CustomTabItem>();
        }

        public void AddNewTab()
        {
            CustomTabs.Add(new CustomTabItem());
        }

        public void Close(int id)
        {
            var toRemove = CustomTabs.FirstOrDefault(x => (x.DataContext as CustomTabViewModel).Id == id);
            CustomTabs.Remove(toRemove);
        }
    }
}
