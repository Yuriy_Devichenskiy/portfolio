﻿using DevExpress.Mvvm.POCO;

namespace MollSzTestTask
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += (s, e) => { this.DataContext = ViewModelSource.Create(() => new MainViewModel()); };
        }
    }
}
