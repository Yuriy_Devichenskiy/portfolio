﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MollSzTestTask
{
    public class CustomTabViewModel
    {
        public virtual int Id { get; set; }

        public virtual int Progress { get; set; }

        public void Loaded()
        {
            Id = DateTime.Now.Millisecond;
            Progress = 0;
            Task.Factory.StartNew(() =>
            {
                while (Progress != 100)
                {
                    Progress++;
                    Thread.Sleep(20);
                }
            });
        }
    }
}
