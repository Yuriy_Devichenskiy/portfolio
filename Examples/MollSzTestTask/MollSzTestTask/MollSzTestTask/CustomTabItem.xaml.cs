﻿using System;
using DevExpress.Mvvm.POCO;

namespace MollSzTestTask
{
    public partial class CustomTabItem
    {
        public int Id { get; set; }

        public CustomTabItem()
        {
            InitializeComponent();
            Loaded += (s, e) =>
            {
                Id = DateTime.Now.Millisecond;
                DataContext = ViewModelSource.Create(() => new CustomTabViewModel());
                (DataContext as CustomTabViewModel).Loaded();
            };
        }
    }
}
