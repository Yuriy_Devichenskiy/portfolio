import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'sum'
})
export class SumPipe implements PipeTransform {
    transform(value: number, args?: any[]): number {
        if (args == null) return;
        let res: number = 0;

        args.forEach(arg => {
            if (arg != undefined)
                res += arg;
        });

        return value + res;
    }
}