import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { CryptonatorData } from '../models/cryptonator.data';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { FixerSymbolsData } from '../models/fixer.data';
import { BlockcypherData } from '../models/blockcypher.data';
import { NanopoolData } from '../models/nanopool.data';

@Injectable()
export class MainService {
    public ethWallet: string = '0x019dae78CBF1c8Ab634b9BA60A84fB0fb5F61d85';
    public btcWallet: string = '1CFZbiRum1fF2SoYSyz3VTRQBQLQz6Kcf';

    constructor(private http: HttpClient) { }

    public getBtcUsdCost(): Promise<CryptonatorData> {
        let apiPath: string = 'https://api.cryptonator.com/api/ticker/btc-usd';

        return this.http.get<CryptonatorData>(apiPath).toPromise();
    }

    public getEthUsdCost(): Promise<CryptonatorData> {
        let apiPath: string = 'https://api.cryptonator.com/api/ticker/eth-usd';
        return this.http.get<CryptonatorData>(apiPath).toPromise();
    }

    public getUsdRurCost(): Promise<FixerSymbolsData> {
        let apiPath: string = 'https://api.fixer.io/latest?base=USD&symbols=RUB';
        return this.http.get<FixerSymbolsData>(apiPath).toPromise();
    }

    public getBtcBalance(): Promise<BlockcypherData> {
        let apiPath = 'https://api.blockcypher.com/v1/btc/main/addrs/' + this.btcWallet + '/balance';
        return this.http.get<BlockcypherData>(apiPath).toPromise();
    }

    public getEthBalance(): Promise<BlockcypherData> {
        let apiPath = 'https://api.blockcypher.com/v1/eth/main/addrs/' + this.ethWallet + '/balance';
        return this.http.get<BlockcypherData>(apiPath).toPromise();
    }

    public getNanopoolData(): Promise<NanopoolData> {
        let apiPath = 'https://api.nanopool.org/v1/eth/user/' + this.ethWallet;        
        return this.http.get<NanopoolData>(apiPath).toPromise();
    }
}