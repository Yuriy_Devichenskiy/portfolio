export class NanopoolData {
    status: boolean;
    data: AccountInfo;
}

export class AccountInfo {
    balance: number;
    unconfirmed_balance: number; //Unconfirmed Balance
    hashrate: number; //Current hashrate
    avgHashrate: NanopoolAvgHashrate;
    worker: object[]; 
}

export class NanopoolAvgHashrate {
    h1: number;//avg hashrate for 1 hour [Mh/s]
    h3: number;//avg for 3 h
    h6: number;//avg for 6 h
    h12: number;//avg for 12 h
    h24: number;//avg for 24 h    
}