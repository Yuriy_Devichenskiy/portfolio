export class CryptonatorData {
    ticker: Ticker;
    timestamp: number;//Timestamp - Update time in Unix timestamp format
    succes: boolean;//Success - True or false
    error: string;//Error - Error description
}

export class Ticker {
    base: string;//Base - Base currency code
    target: string;//Target - Target currency code
    price: number;//Price - Volume-weighted price
    volume: number;//Volume - Total trade volume for the last 24 hours
    change: number;//Change - Past hour price change
}