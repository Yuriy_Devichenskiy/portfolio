export class FixerSymbolsData {
    base: string;
    date: Date;
    rates: FixerRates;
}

export class FixerRates {
    RUB: number;
}