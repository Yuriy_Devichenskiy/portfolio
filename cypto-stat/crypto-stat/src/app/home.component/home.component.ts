import { Component, OnInit } from '@angular/core';
import { MainService } from '../common.services/main.service';

@Component({
    selector: 'home-comp',
    templateUrl: 'home.view.html',
    styleUrls: ['home.styles.css']
})
export class HomeComponent implements OnInit {

    public btcBalance: number;
    public btcUsdCost: number;
    public btcProfit: number;

    public ethBalance: number;
    public ethPoolBalance: number;
    public ethUsdCost: number;
    public ethProfit: number;
    public ethPoolProfit: number;
    public ethSixAvg : number;

    public ethWallet: string;
    public btcWallet: string;

    public usdRurCost: number;
    public eurRurCost: number;

    constructor(private mainService: MainService) {

    }

    ngOnInit() {
        this.update();
    }

    update() {
        this.skipCosts();

        this.mainService.getUsdRurCost().then(usdRurCost => {
            this.usdRurCost = usdRurCost.rates.RUB;

            this.mainService.getBtcBalance().then(btcBalance => {
                this.btcBalance = btcBalance.balance * 0.00000001;
                this.mainService.getBtcUsdCost().then(cost => {
                    this.btcUsdCost = cost.ticker.price;
                    this.btcProfit = this.btcBalance * this.btcUsdCost * this.usdRurCost;
                });
            });

            this.mainService.getEthBalance().then(ethBalance => {
                
                this.ethBalance = ethBalance.balance * 0.000000000000000001;

                this.mainService.getEthUsdCost().then(cost => {

                    this.ethUsdCost = cost.ticker.price;                    
                    this.ethProfit = this.ethBalance * this.ethUsdCost * this.usdRurCost;
                    
                    this.mainService.getNanopoolData().then(miningData => {

                        this.ethPoolBalance = miningData.data.balance  * 1;
                        this.ethPoolProfit = this.ethPoolBalance * this.ethUsdCost * this.usdRurCost;
                        this.ethSixAvg = miningData.data.avgHashrate.h6;
                    });
                });
            });
        });

        this.mainService.getBtcUsdCost().then(value => {
            this.btcUsdCost = value.ticker.price;
        });
    }

    private skipCosts() {
        //bitcoin
        this.btcBalance = 0;
        this.btcUsdCost = 0;
        this.btcProfit = 0;

        //ether
        this.ethBalance = 0;
        this.ethPoolBalance = 0;
        this.ethUsdCost = 0;
        this.ethProfit = 0;

        //currencies
        this.usdRurCost = 0;
        this.eurRurCost = 0;
    }
}