import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home.component/home.component';
import { EthComponent } from './eth.component/eth.component';
import { JournalComponent } from './journal.component/journal.component';
import { BtcComponent } from './btc.component/btc.component';

export const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'eth', component: EthComponent },
    {path: 'btc', component: BtcComponent },
    { path: 'journal', component: JournalComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }