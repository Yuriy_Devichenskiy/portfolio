import { Component, OnInit } from '@angular/core';
import { MainService } from '../common.services/main.service';

@Component({
    selector: 'eth-comp',
    templateUrl: 'eth.view.html',
    styleUrls: ['eth.styles.css']
})
export class EthComponent implements OnInit {

    balance: number;
    oneHourHashrate: number;
    sixHourHashrate: number;
    dayHashrate: number;

    constructor(private mainService: MainService) { }

    ngOnInit() {
        this.update();
    }

    update() {
        this.skipData();

        this.mainService.getNanopoolData().then(data=>{

            console.log(data);

            this.balance = data.data.balance;
            this.oneHourHashrate = data.data.avgHashrate.h1;
            this.sixHourHashrate = data.data.avgHashrate.h6;
            this.dayHashrate = data.data.avgHashrate.h24;
        });
    }

    skipData() {
        this.balance = 0;
        this.oneHourHashrate = 0;
        this.sixHourHashrate = 0;
        this.dayHashrate = 0;
    }
}