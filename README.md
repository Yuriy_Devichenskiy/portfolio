# Портфолио #

Несколько небольших проектов для подтверждения навыков.

* GameStore - ASP.Net проект.
* MessengerLib - сборка WPF с контролом.
* eth-stat-app - проект на Angular для мониторинга состояния моего счета ETH.
* eth-stat-hybrid - альтернативная версия Eth-stat на Ionic.

* Examples\MollSzTestTask - тестовый проект - многопоточность в C#.
* Examples\FastFoodTest - тестовый проект - Web-приложение на Asp.Net.
* Examples\Calculator - тестовый проект на .NetCore.

###yuvdevichenskiy@gmail.com###