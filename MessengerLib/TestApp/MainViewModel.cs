﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using MessengerLib.Model;

namespace TestApp
{
    public class MainViewModel
    {
        public ObservableCollection<string> Persons { get; set; }

        public MainViewModel()
        {
            ClientMessages = new ObservableCollection<Message>();
            
            Persons = new ObservableCollection<string>(new List<string>()
            {
                "Иванов",
                "Петров",
                "Сидоров"
            });

            Commentator = Persons.FirstOrDefault();
        }

        public virtual ObservableCollection<Message> ClientMessages { get; set; }

        public virtual string Commentator { get; set; }

        public virtual Message EditingMessage { get; set; }

        public void Edit(Message messageToEdit)
        {
            if (messageToEdit != null)
                MessageBox.Show(messageToEdit.Text);
            else MessageBox.Show("null!");
        }

        public void Commit(Message commitedMessage)
        {
            if (commitedMessage != null)
                MessageBox.Show(commitedMessage.Text);
            else MessageBox.Show("null!");
        }
    }
}
