﻿using System.Windows;
using System.Windows.Media;
using DevExpress.Mvvm.POCO;

namespace TestApp
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = ViewModelSource.Create(() => new MainViewModel());
            TestBorder.BorderBrush = Brushes.Coral;
            TestBorder.BorderThickness = new Thickness{ Bottom = 1, Left = 1, Right = 1, Top = 1};
        }
    }
}
