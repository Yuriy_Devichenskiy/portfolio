﻿using System.Collections.Generic;
using System.Threading;
using DevExpress.Mvvm.POCO;
using MessengerLib.Model;

namespace CustomBehaviorTest
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = ViewModelSource.Create(() => new MainViewModel());
        }
    }

    public class MainViewModel
    {
        public List<Message> DataSource { get; set; }

        public MainViewModel()
        {
            DataSource = new List<Message>();
        }

        public void Loaded()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                DataSource.Add(new Message(){Text = "VAsisa"});
                this.RaisePropertyChanged(x=>x.DataSource);
            }
        }
    }
}
