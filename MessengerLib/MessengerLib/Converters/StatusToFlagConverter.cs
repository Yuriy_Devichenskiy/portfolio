﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace MessengerLib.Converters
{
    public class StatusToFlagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                if (string.IsNullOrEmpty(value.ToString())) return 0;
                return SwitchValue(Int32.Parse(value.ToString()));
            }

            return SwitchValue(Int32.Parse(value.ToString()));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return 0;

            if (value is Image)
            {
                var img = value as Image;

                if (img.Source == null)
                    return 0;

                return SwitchBack(img.Source.ToString());
            }
            return SwitchValue(Int32.Parse(value.ToString()));
        }

        private int SwitchBack(string imageSource)
        {
            switch (imageSource)
            {
                case "pack://application:,,,/MessengerLib;component/Images/flag_orange.png":
                    return 0;
                case "pack://application:,,,/MessengerLib;component/Images/flag_blue.png":
                    return 1;
                case "pack://application:,,,/MessengerLib;component/Images/flag_red.png":
                    return 2;
                case "pack://application:,,,/MessengerLib;component/Images/flag_green.png":
                    return 3;
                default:
                    throw new Exception("Wrong status in converter");
            }
        }

        private string SwitchValue(int status)
        {
            switch (status)
            {
                case 0:
                    return "pack://application:,,,/MessengerLib;component/Images/flag_orange.png";
                case 1:
                    return "pack://application:,,,/MessengerLib;component/Images/flag_blue.png";
                case 2:
                    return "pack://application:,,,/MessengerLib;component/Images/flag_red.png";
                case 3:
                    return "pack://application:,,,/MessengerLib;component/Images/flag_green.png";
                default:
                    return null;
            }
        }
    }
}
