﻿using System;

namespace MessengerLib.Model
{
    public class Message
    {
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        public virtual DateTime Date { get; set;}
        public virtual string CommentatorName { get; set; }
        public virtual int Status { get; set; }
        public virtual bool Recieved { get; set; }
    }
}
