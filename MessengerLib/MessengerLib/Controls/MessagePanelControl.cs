﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using MessengerLib.Model;

namespace MessengerLib.Controls
{
    public class MessagePanelControl : Control
    {
        public static readonly DependencyProperty DataSourceProperty = DependencyProperty.Register(
            "DataSource", typeof(IEnumerable<Message>), typeof(MessagePanelControl), new PropertyMetadata(default(IEnumerable<Message>)));

        public IEnumerable<Message> DataSource
        {
            get { return (IEnumerable<Message>)GetValue(DataSourceProperty); }
            set { SetValue(DataSourceProperty, value); }
        }

        public static readonly DependencyProperty EditMessagePanelCommandProperty =
            DependencyProperty.Register("EditMessagePanelCommand", typeof(ICommand),
            typeof(MessagePanelControl));

        public ICommand EditMessagePanelCommand
        {
            get { return (ICommand)GetValue(EditMessagePanelCommandProperty); }
            set { SetValue(EditMessagePanelCommandProperty, value); }
        }
        static MessagePanelControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MessagePanelControl),
                new FrameworkPropertyMetadata(typeof(MessagePanelControl)));
        }

        public MessagePanelControl()
        {
            var menuItem = new Label { Content = "Редактировать" };
            menuItem.MouseDown += (sender, args) => RaiseEdit((Label)sender, args);

            Resources.Add("_Menu",
                new ContextMenu
                {
                    Items = { menuItem }
                });
        }

        private void RaiseEdit(Label sender, MouseButtonEventArgs args)
        {
            var menu = sender.Parent;
            var popup = ((ContextMenu)menu).Parent;
            var border = ((Popup)popup).PlacementTarget;

            var dock = ((Border)border).Child;

            var borderToLight = (Border)((DockPanel)dock).Children[2];

            borderToLight.BorderThickness = new Thickness(2);

            //border
            var dc = border.GetValue(DataContextProperty);

            var message = dc as Message;

            if (message != null)
                EditMessagePanelCommand.Execute(message);
        }
    }
}
