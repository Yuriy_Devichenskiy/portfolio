﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MessengerLib.Model;

namespace MessengerLib.Controls
{
    public class MessageInputControl : Control
    {
        public static readonly DependencyProperty StatusesProperty = DependencyProperty.Register(
            "Statuses", typeof (List<int>), typeof (MessageInputControl), new PropertyMetadata(default(List<int>)));

        public List<int> Statuses
        {
            get { return (List<int>) GetValue(StatusesProperty); }
            set { SetValue(StatusesProperty, value); }
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(Message),
                typeof(MessageInputControl));

        public Message Message
        {
            get { return (Message)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        public static readonly DependencyProperty SaveCommandProperty =
            DependencyProperty.Register("SaveCommand", typeof(ICommand),
                typeof(MessageInputControl));

        public ICommand SaveCommand
        {
            get { return (ICommand)GetValue(SaveCommandProperty); }
            set { SetValue(SaveCommandProperty, value); }
        }

        static MessageInputControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MessageInputControl),
                new FrameworkPropertyMetadata(typeof(MessageInputControl)));
        }

        public MessageInputControl()
        {
            this.KeyDown += MessageInputControl_KeyDown;
            Statuses = new List<int> { 0, 1, 2, 3 };
        }

        void MessageInputControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control)
                if (e.Key == Key.Return)
                {
                    if (SaveCommand != null)
                    {
                        SaveCommand.Execute(null);
                    }
                }
        }
    }
}
