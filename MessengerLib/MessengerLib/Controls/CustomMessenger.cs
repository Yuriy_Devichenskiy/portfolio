﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using MessengerLib.Model;

namespace MessengerLib.Controls
{
    public class CustomMessenger : Control
    {
        #region | Mode Property |

        public static readonly DependencyProperty EditModeOnProperty = DependencyProperty.Register(
            "EditModeOn", typeof(bool), typeof(CustomMessenger), new PropertyMetadata(default(bool)));

        public bool EditModeOn
        {
            get { return (bool)GetValue(EditModeOnProperty); }
            set { SetValue(EditModeOnProperty, value); }
        }

        #endregion

        #region | Messages Property |

        public static readonly DependencyProperty MessagesProperty = DependencyProperty.Register(
            "Messages", typeof(ObservableCollection<Message>), typeof(CustomMessenger), new PropertyMetadata(default(ObservableCollection<Message>)));

        public ObservableCollection<Message> Messages
        {
            get { return (ObservableCollection<Message>)GetValue(MessagesProperty); }
            set { SetValue(MessagesProperty, value); }
        }

        #endregion

        #region | Message Property |

        public static readonly DependencyProperty MessageProperty = DependencyProperty.Register(
            "Message", typeof(Message), typeof(CustomMessenger), new PropertyMetadata(default(Message)));

        public Message Message
        {
            get { return (Message)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        #endregion

        #region | CommitCommand Property |

        public static readonly DependencyProperty CommitCommandProperty = DependencyProperty.Register(
            "CommitCommand", typeof(ICommand), typeof(CustomMessenger), new PropertyMetadata(default(ICommand)));

        public ICommand CommitCommand
        {
            get { return (ICommand)GetValue(CommitCommandProperty); }
            set { SetValue(CommitCommandProperty, value); }
        }

        #endregion

        #region | SendCommand Property |

        public static readonly DependencyProperty SendCommandProperty = DependencyProperty.Register(
            "SendCommand", typeof(ICommand), typeof(CustomMessenger), new PropertyMetadata(default(ICommand)));

        protected ICommand SendCommand
        {
            get { return (ICommand)GetValue(SendCommandProperty); }
            set { SetValue(SendCommandProperty, value); }
        }

        #endregion

        #region | protected EditMessageCommand Property |

        public static readonly DependencyProperty EditMessageCommandProperty = DependencyProperty.Register(
            "EditMessageCommand", typeof(ICommand), typeof(CustomMessenger), new PropertyMetadata(default(ICommand)));

        protected ICommand EditMessageCommand
        {
            get { return (ICommand)GetValue(EditMessageCommandProperty); }
            set { SetValue(EditMessageCommandProperty, value); }
        }

        #endregion

        #region | EditCommand Property |

        public static readonly DependencyProperty EditCommandProperty = DependencyProperty.Register(
            "EditCommand", typeof(ICommand), typeof(CustomMessenger), new PropertyMetadata(default(ICommand)));

        /// <summary>
        /// Got Message Parameter
        /// </summary>
        public ICommand EditCommand
        {
            get { return (ICommand)GetValue(EditCommandProperty); }
            set { SetValue(EditCommandProperty, value); }
        }

        #endregion

        #region | CommentatorName Property |

        public static readonly DependencyProperty CommentatorNameProperty = DependencyProperty.Register(
            "CommentatorName", typeof(string), typeof(CustomMessenger), new PropertyMetadata(default(string),
                (o, args) =>
                {
                    var messenger = (CustomMessenger)o;

                    if (messenger.Messages != null && messenger.Messages.Any())
                        foreach (var message in messenger.Messages)
                        {
                            message.Recieved = message.CommentatorName != args.NewValue.ToString();
                        }
                }));

        public string CommentatorName
        {
            get { return (string)GetValue(CommentatorNameProperty); }
            set { SetValue(CommentatorNameProperty, value); }
        }

        #endregion

        #region | Constructors |

        static CustomMessenger()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CustomMessenger),
                new FrameworkPropertyMetadata(typeof(CustomMessenger)));
        }

        public CustomMessenger()
        {
            Message = ViewModelSource.Create(() => new Message());

            Message messageToSend;

            SendCommand = new DelegateCommand(() =>
            {
                if (!EditModeOn)
                {
                    if (string.IsNullOrEmpty(Message.Text))
                        return; //Сброс пустого ввода
                    Message.Date = DateTime.Now;
                    Message.Recieved = false;
                    Message.CommentatorName = CommentatorName;
                    Messages.Add(Message);
                    messageToSend = Message;
                }
                else
                    messageToSend = Message;

                Message = ViewModelSource.Create(() => new Message());
                if (CommitCommand != null)
                {
                    CommitCommand.Execute(messageToSend);
                }
            });

            EditMessageCommand = new DelegateCommand<Message>((mes) =>
            {
                if (mes == null)
                    return;
                //EditCommand.Execute(mes);
                Message = mes;
                EditModeOn = true;
            });
        }

        #endregion
    }
}
