﻿using System;
using System.Text.RegularExpressions;
using System.Threading;

namespace SimpleCSharpTests
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncDelegateTest();
            Console.ReadKey();
        }        

        static void RegexTest()
        {
            var s = "Бык тупогуб, тупогубенький бычок, у быка губа бела была тупа отупевший";

            Regex regex = new Regex(@"туп(\w*)");
            var coll = regex.Matches(s);
        }

        static void AsyncDelegateTest()
        {
            Test del = Test;
            IAsyncResult ar = del.BeginInvoke(0, null, null);
            Console.Write('_');
            Thread.Sleep(200);
            Console.Write('_');
            del.EndInvoke(ar);
        }

        static void Test(int t)
        {
            Console.Write('*');
            Thread.Sleep(500);
            Console.Write('*');
        }
    }

    public delegate void Test(int t);
}
