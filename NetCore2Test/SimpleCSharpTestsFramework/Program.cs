﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleCSharpTestsFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncDelegateTest();
            Console.ReadKey();
        }

        static void AsyncDelegateTest()
        {
            Test del = Test;
            IAsyncResult ar = del.BeginInvoke(0, null, null);
            Console.Write('_');
            Thread.Sleep(200);
            Console.Write('_');
            del.EndInvoke(ar);
        }

        static void Test(int t)
        {
            Console.Write('*');
            Thread.Sleep(500);
            Console.Write('*');
        }
    }

    public delegate void Test(int t);
}
