﻿using System;
using System.Collections.Generic;

namespace WebApplication1
{
    public partial class AdStatus
    {
        public AdStatus()
        {
            Ads = new HashSet<Ads>();
        }

        public int Id { get; set; }
        public string[] Name { get; set; }
        public string[] Code { get; set; }

        public ICollection<Ads> Ads { get; set; }
    }
}
