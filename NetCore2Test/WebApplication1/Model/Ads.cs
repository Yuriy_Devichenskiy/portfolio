﻿using System;
using System.Collections.Generic;

namespace WebApplication1
{
    public partial class Ads
    {
        public int Id { get; set; }
        public string[] Title { get; set; }
        public string Text { get; set; }
        public int Price { get; set; }
        public bool? CanBargain { get; set; }
        public string Location { get; set; }
        public int AdStatusId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int UserId { get; set; }

        public AdStatus AdStatus { get; set; }
        public Users User { get; set; }
    }
}
