﻿using Microsoft.EntityFrameworkCore;

namespace WebApplication1
{
    public partial class jdevlmlyContext : DbContext
    {
        public virtual DbSet<AccountInfo> AccountInfo { get; set; }
        public virtual DbSet<Ads> Ads { get; set; }
        public virtual DbSet<AdStatus> AdStatus { get; set; }
        public virtual DbSet<GoodCategories> GoodCategories { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        public jdevlmlyContext(DbContextOptions<jdevlmlyContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("btree_gin")
                .HasPostgresExtension("btree_gist")
                .HasPostgresExtension("citext")
                .HasPostgresExtension("cube")
                .HasPostgresExtension("dblink")
                .HasPostgresExtension("dict_int")
                .HasPostgresExtension("dict_xsyn")
                .HasPostgresExtension("earthdistance")
                .HasPostgresExtension("fuzzystrmatch")
                .HasPostgresExtension("hstore")
                .HasPostgresExtension("intarray")
                .HasPostgresExtension("ltree")
                .HasPostgresExtension("pg_stat_statements")
                .HasPostgresExtension("pg_trgm")
                .HasPostgresExtension("pgcrypto")
                .HasPostgresExtension("pgrowlocks")
                .HasPostgresExtension("pgstattuple")
                .HasPostgresExtension("plv8")
                .HasPostgresExtension("tablefunc")
                .HasPostgresExtension("unaccent")
                .HasPostgresExtension("uuid-ossp")
                .HasPostgresExtension("xml2");

            modelBuilder.Entity<AccountInfo>(entity =>
            {
                entity.ToTable("account_info");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Fio).HasColumnName("FIO");

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasColumnName("location");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("char(12)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AccountInfo)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_id");
            });

            modelBuilder.Entity<Ads>(entity =>
            {
                entity.ToTable("ads");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdStatusId)
                    .HasColumnName("ad_status_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CanBargain)
                    .HasColumnName("can_bargain")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("date")
                    .HasDefaultValueSql("('now'::text)::date");

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasColumnName("location");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasColumnName("text");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasColumnType("varchar[]");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("date")
                    .HasDefaultValueSql("('now'::text)::date");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.AdStatus)
                    .WithMany(p => p.Ads)
                    .HasForeignKey(d => d.AdStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ad_status_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Ads)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_id");
            });

            modelBuilder.Entity<AdStatus>(entity =>
            {
                entity.ToTable("ad_status");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("bpchar[]");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar[]");
            });

            modelBuilder.Entity<GoodCategories>(entity =>
            {
                entity.ToTable("good_categories");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("bpchar[]");

                entity.Property(e => e.GoodCategoryId)
                    .HasColumnName("good_category_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar[]");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('\"Users_id_seq\"'::regclass)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("date");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login")
                    .HasColumnType("varchar[]");

                entity.Property(e => e.PassHash)
                    .IsRequired()
                    .HasColumnName("pass_hash");

                entity.Property(e => e.Token).HasColumnName("token");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("date");
            });

            modelBuilder.HasSequence("Users_id_seq");
        }
    }
}
