﻿using System;
using System.Collections.Generic;

namespace WebApplication1
{
    public partial class Users
    {
        public Users()
        {
            AccountInfo = new HashSet<AccountInfo>();
            Ads = new HashSet<Ads>();
        }

        public int Id { get; set; }
        public string[] Login { get; set; }
        public string PassHash { get; set; }
        public string Token { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public ICollection<AccountInfo> AccountInfo { get; set; }
        public ICollection<Ads> Ads { get; set; }
    }
}
