﻿using System;
using System.Collections.Generic;

namespace WebApplication1
{
    public partial class GoodCategories
    {
        public int Id { get; set; }
        public int GoodCategoryId { get; set; }
        public string[] Name { get; set; }
        public string[] Code { get; set; }
    }
}
