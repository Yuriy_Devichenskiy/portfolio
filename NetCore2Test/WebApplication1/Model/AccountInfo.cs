﻿using System;
using System.Collections.Generic;

namespace WebApplication1
{
    public partial class AccountInfo
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string Fio { get; set; }
        public char Phone { get; set; }
        public int UserId { get; set; }

        public Users User { get; set; }
    }
}
