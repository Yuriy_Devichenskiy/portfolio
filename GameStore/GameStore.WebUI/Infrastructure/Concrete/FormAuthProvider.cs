﻿using System.Web.Security;
using GameStore.WebUI.Infrastructure.Abstract;

namespace GameStore.WebUI.Infrastructure.Concrete
{
    public class FormAuthProvider : IAuthProvider
    {
        public bool Authenticate(string username, string password)
        {
            var result = FormsAuthentication.Authenticate(username, password);
            if(result)
                FormsAuthentication.SetAuthCookie(username, false);
            return result;
        }
    }
}