﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GameStore.Domain.Entities
{
    public class Game
    {
        [HiddenInput(DisplayValue = false)]
        public int GameId { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Введите название игры")]
        public string Name { get; set; }
        
        [DataType(DataType.MultilineText)]
        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Введите описание игры")]
        public string Description { get; set; }

        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Укажите категорию для игры")]
        public string Category { get; set; }

        [Display(Name = "Цена (руб)")]
        [Required(ErrorMessage = "Введите цену")]
        [Range(0.01, Double.MaxValue, ErrorMessage = "Введите положительное значение для игры")]
        public decimal Price { get; set; }

        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
    }
}