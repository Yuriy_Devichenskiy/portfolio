﻿using System.Collections.Generic;
using System.Linq;
using GameStore.Domain.Abstract;
using GameStore.Domain.Entities;
using GameStore.WebUI.Controllers;
using GameStore.WebUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GameStore.UnitTests
{
    [TestClass]
    public class CartTests
    {
        [TestMethod]
        public void Can_Add_New_Lines()
        {
            //arrange
            var game1 = new Game { GameId = 1, Name = "Игра1" };
            var game2 = new Game { GameId = 2, Name = "Игра2" };

            var cart = new Cart();

            //act

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            var results = cart.Lines.ToList();

            //assert
            Assert.AreEqual(results.Count(), 2);
            Assert.AreEqual(results[0].Game, game1);
            Assert.AreEqual(results[1].Game, game2);
        }

        [TestMethod]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            //arrange
            var game1 = new Game { GameId = 1, Name = "Игра1" };
            var game2 = new Game { GameId = 2, Name = "Игра2" };

            var cart = new Cart();

            //act
            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            cart.AddItem(game1, 5);
            var results = cart.Lines.OrderBy(c => c.Game.GameId).ToList();

            //assert
            Assert.AreEqual(results.Count(), 2);
            Assert.AreEqual(results[0].Quantity, 6);
            Assert.AreEqual(results[1].Quantity, 1);
        }

        [TestMethod]
        public void Can_RemoveLines()
        {
            //arrange
            var game1 = new Game { GameId = 1, Name = "Игра1" };
            var game2 = new Game { GameId = 2, Name = "Игра2" };
            var game3 = new Game { GameId = 3, Name = "Игра3" };

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 4);
            cart.AddItem(game3, 2);
            cart.AddItem(game2, 1);

            //act
            cart.RemoveLine(game2);

            //assert
            Assert.AreEqual(cart.Lines.Count(c => c.Game == game2), 0);
            Assert.AreEqual(cart.Lines.Count(), 2);
        }

        [TestMethod]
        public void Calculate_Cart_Total()
        {
            //arrange
            var game1 = new Game { GameId = 1, Name = "Игра1", Price = 100 };
            var game2 = new Game { GameId = 2, Name = "Игра2", Price = 55 };

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            cart.AddItem(game1, 5);
            //act
            var result = cart.ComputeTotalValue();

            //assert
            Assert.AreEqual(result, 655);
        }

        [TestMethod]
        public void Can_Clear_Contents()
        {
            //arrange
            var game1 = new Game { GameId = 1, Name = "Игра1", Price = 100 };
            var game2 = new Game { GameId = 2, Name = "Игра2", Price = 55 };

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            cart.AddItem(game1, 5);

            //act
            cart.Clear();

            //assert
            Assert.AreEqual(cart.Lines.Count(), 0);
        }

        [TestMethod]
        public void Can_Add_To_Cart()
        {
            //arrange
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new List<Game>
            {
                new Game {GameId = 1, Name = "Игра1", Category = "Кат1"},
            }.AsQueryable());

            var cart = new Cart();

            var controller = new CartController(mock.Object, null);

            //act
            controller.AddToCart(cart, 1, null);

            //assert
            Assert.AreEqual(cart.Lines.Count(), 1);
            Assert.AreEqual(cart.Lines.ToList()[0].Game.GameId, 1);
        }

        [TestMethod]
        public void Adding_Game_To_Cart_Goes_To_Cart_Screen()
        {
            //arrange
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new List<Game>
            {
                new Game {GameId = 1, Name = "Игра1", Category = "Кат1"},
            }.AsQueryable());

            var cart = new Cart();

            var controller = new CartController(mock.Object, null);

            //act 
            var result = controller.AddToCart(cart, 2, "myUrl");

            //assert
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
        }

        [TestMethod]
        public void Can_View_Cart_Contents()
        {
            //arrange
            var cart = new Cart();
            var target = new CartController(null, null);

            //act
            var result = (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;

            //assert
            Assert.AreSame(result.Cart, cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }

        [TestMethod]
        public void Cannot_Checkout_Empty_Cart()
        {
            //arrange
            var mock = new Mock<IOrderProcessor>();
            var cart = new Cart();
            var shippingDetails = new ShippingDetails();
            var controller = new CartController(null, mock.Object);

            //act
            var result = controller.Checkout(cart, shippingDetails);

            //assert 
            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never);
            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Cannot_Checkout_Invalid_ShippingDetails()
        {
            //arrange
            var mock = new Mock<IOrderProcessor>();
            var cart = new Cart();
            cart.AddItem(new Game(), 1);
            var controller = new CartController(null, mock.Object);
            controller.ModelState.AddModelError("error", "error");

            //act
            var result = controller.Checkout(cart, new ShippingDetails());

            //assert
            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never);
            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Can_Checkout_And_Submit_Order()
        {
            //arrange
            var mock = new Mock<IOrderProcessor>();
            var cart = new Cart();
            cart.AddItem(new Game(), 1);
            var controller = new CartController(null, mock.Object);

            //act
            var result = controller.Checkout(cart, new ShippingDetails());

            //assert
            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Once);
            Assert.AreEqual("Completed", result.ViewName);
            Assert.AreEqual(true, result.ViewData.ModelState.IsValid);
        }
    }
}
